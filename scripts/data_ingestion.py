import os
import pandas as pd

def ingest_data(file_path, output_dir):
    """Ingest data from the specified file path and save it to the output directory."""
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    
    dtype = {
        "COD_MEDIDOR": "str",
        "CONSUMO": "int64",
        "DES_CANAL": "str",            
        "TIPO": "str", 
        "HORA": "int64",                                           
        "NIVEL TENSION": "str",            
        "VALOR KWh TARIFA PLANA": "float64",
        "TIPO DIA": "str"
    }
    
    data = pd.read_csv(file_path, index_col=None, decimal=".", encoding='latin1', sep=';', 
                       dayfirst=True, parse_dates=["FECHA Y HORA", "FECHA"], dtype=dtype, low_memory=False)
    output_file_path = os.path.join(output_dir, 'raw_data.csv')
    data.to_csv(output_file_path, index=False)
    print(f"Data ingested and saved to {output_file_path}")

if __name__ == "__main__":
    file_path = 'data/source/sample01.csv'  # https://raw.githubusercontent.com/ogduartev/energyDataScience/main/data/distribution/sample01.csv
    output_dir = 'data/raw'
    ingest_data(file_path, output_dir)