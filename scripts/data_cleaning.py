import os
import pandas as pd

def clean_data(input_file_path, output_file_path):
    """Clean the data and save the cleaned data to the output file path."""
    output_dir = 'data/clean/'
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    
    dtype = {
        "COD_MEDIDOR": "str",
        "CONSUMO": "int64",
        "DES_CANAL": "str",            
        "TIPO": "str", 
        "HORA": "int64",                                           
        "NIVEL TENSION": "str",            
        "VALOR KWh TARIFA PLANA": "float64",
        "TIPO DIA": "str"
    }
    
    data = pd.read_csv(input_file_path, parse_dates=["FECHA Y HORA", "FECHA"], dtype=dtype, dayfirst=True, decimal=".")
    
    data = data.dropna()  # Drop rows with missing values
    data = data.drop_duplicates()  # Drop duplicate rows

    # Order the data by 'FECHA' and then by 'FECHA Y HORA'
    data = data.sort_values(by=['FECHA', 'FECHA Y HORA'])
    
    data.to_csv(output_file_path, index=False)
    print(f"Data cleaned and saved to {output_file_path}")

if __name__ == "__main__":
    input_file_path = 'data/raw/raw_data.csv'
    output_file_path = 'data/clean/cleaned_data.csv'
    clean_data(input_file_path, output_file_path)