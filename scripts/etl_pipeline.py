import os
import pandas as pd
from sklearn.preprocessing import StandardScaler, MinMaxScaler, RobustScaler

def load_data(file_path):
    """Load data from the specified file path."""
    dtype = {
        "COD_MEDIDOR": "str",
        "CONSUMO": "int64",
        "DES_CANAL": "str",            
        "TIPO": "str", 
        "HORA": "int64",                                           
        "NIVEL TENSION": "str",            
        "VALOR KWh TARIFA PLANA": "float64",
        "TIPO DIA": "str"
    }
    return pd.read_csv(file_path, parse_dates=["FECHA Y HORA", "FECHA"], dtype=dtype, dayfirst=True, decimal=".")

def normalize_data(data, columns, method='standard'):
    """Normalize the specified columns using the specified method."""
    if method == 'standard':
        scaler = StandardScaler()
    elif method == 'minmax':
        scaler = MinMaxScaler()
    elif method == 'robust':
        scaler = RobustScaler()
    else:
        raise ValueError("Invalid normalization method. Choose 'standard', 'minmax', or 'robust'.")
    
    scaled_data = scaler.fit_transform(data[columns])
    scaled_df = pd.DataFrame(scaled_data, columns=columns)
    
    # Combine the non-scaled columns with the scaled columns
    non_scaled_cols = data.drop(columns, axis=1)
    final_data = pd.concat([non_scaled_cols, scaled_df], axis=1)
    
    return final_data

def save_data(data, file_path):
    """Save data to the specified file path."""
    data.to_csv(file_path, index=False)
    print(f"Data saved to {file_path}")

if __name__ == "__main__":
    input_file_path = 'data/clean/cleaned_data.csv'
    output_dir = 'data/processed'
    
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    
    data = load_data(input_file_path)
    
    # Columns to be normalized
    columns_to_normalize = ['CONSUMO', 'VALOR KWh TARIFA PLANA']
    
    # Normalize data using different methods
    methods = ['standard', 'minmax', 'robust']
    for method in methods:
        normalized_data = normalize_data(data, columns_to_normalize, method)
        output_file_path = os.path.join(output_dir, f'normalized_data_{method}.csv')
        save_data(normalized_data, output_file_path)