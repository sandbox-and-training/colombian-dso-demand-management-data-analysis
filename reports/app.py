import streamlit as st
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler, MinMaxScaler, RobustScaler

# Set up the Streamlit page configuration
st.set_page_config(page_title='Demand Management Analysis', layout='wide')

# Function to load data
@st.cache_data
def load_data():
    cleaned_data = pd.read_csv('data/clean/cleaned_data.csv', parse_dates=["FECHA Y HORA", "FECHA"], dayfirst=True)
    normalized_data_standard = pd.read_csv('data/processed/normalized_data_standard.csv')
    normalized_data_minmax = pd.read_csv('data/processed/normalized_data_minmax.csv')
    normalized_data_robust = pd.read_csv('data/processed/normalized_data_robust.csv')
    return cleaned_data, normalized_data_standard, normalized_data_minmax, normalized_data_robust

# Load the data
cleaned_data, normalized_data_standard, normalized_data_minmax, normalized_data_robust = load_data()

# Sidebar for navigation
st.sidebar.title("Navigation")
page = st.sidebar.radio("Go to", ["Introduction", "EDA", "Clustering Analysis"])

# Introduction Page
if page == "Introduction":
    st.title("Demand Management Analysis")
    st.markdown("""
    This app demonstrates the analysis performed on the demand management data collected from a DSO in Colombia. The main objectives are to improve the utilization of distribution assets, reduce peak hour congestion, and explore potential differential tariff schemes.
    """)

# EDA Page
if page == "EDA":
    st.title("Exploratory Data Analysis (EDA)")

    # Display the data overview
    st.subheader("Data Overview")
    st.write(cleaned_data.head())
    st.write(cleaned_data.describe())

    # Plot distribution of consumption
    st.subheader("Distribution of Consumption")
    fig, axs = plt.subplots(1, 2, figsize=(14, 6))
    
    sns.histplot(cleaned_data['CONSUMO'], bins=50, kde=True, ax=axs[0])
    axs[0].set_title('Distribution of CONSUMO')
    
    sns.histplot(cleaned_data['VALOR KWh TARIFA PLANA'], bins=50, kde=True, ax=axs[1])
    axs[1].set_title('Distribution of VALOR KWh TARIFA PLANA')

    st.pyplot(fig)

# Clustering Analysis Page
if page == "Clustering Analysis":
    st.title("Clustering Analysis")

    # Function to plot the elbow curve
    def plot_elbow_curve(data, max_k=15):
        sse = []
        for k in range(1, max_k+1):
            kmeans = KMeans(n_clusters=k, random_state=42)
            kmeans.fit(data)
            sse.append(kmeans.inertia_)
        
        fig, ax = plt.subplots()
        ax.plot(range(1, max_k+1), sse, marker='o')
        ax.set_xlabel('Number of Clusters')
        ax.set_ylabel('SSE')
        ax.set_title('Elbow Curve')
        st.pyplot(fig)

    st.subheader("Finding the Optimal Number of Clusters")
    st.markdown("Using the elbow method to find the optimal number of clusters.")
    plot_elbow_curve(normalized_data_standard[['CONSUMO', 'VALOR KWh TARIFA PLANA']])

    st.subheader("Applying KMeans Clustering")
    optimal_clusters = st.slider("Select the number of clusters", 2, 15, 6)
    
    kmeans = KMeans(n_clusters=optimal_clusters, random_state=42)
    clusters = kmeans.fit_predict(normalized_data_standard[['CONSUMO', 'VALOR KWh TARIFA PLANA']])
    
    # Add the cluster labels to the original data
    cleaned_data['Cluster'] = clusters

    # Visualize the clusters
    fig, ax = plt.subplots(figsize=(10, 6))
    sns.scatterplot(data=cleaned_data, x='CONSUMO', y='VALOR KWh TARIFA PLANA', hue='Cluster', palette='viridis', ax=ax)
    ax.set_title('Clusters of Users based on Consumption Patterns')
    st.pyplot(fig)

    st.subheader("Cluster Summary")
    st.write(cleaned_data.groupby('Cluster').agg({'CONSUMO': ['mean', 'std'], 'VALOR KWh TARIFA PLANA': ['mean', 'std']}))

# Conclusions Page (You can add a separate page or include this in the Clustering Analysis section)
st.title("Conclusions")
st.markdown("""
In this analysis, we have undertaken a comprehensive study of the demand management data. Here are the key findings and insights derived from our analysis:

1. **Data Overview**: The dataset contains detailed information on energy consumption. Through initial exploration, we identified that the dataset is clean and well-structured, with no missing values or duplicates after the cleaning process.

2. **Consumption Distribution**: The distribution of the 'CONSUMO' and 'VALOR KWh TARIFA PLANA' columns indicates variability in energy consumption among different users. The histograms showed that the consumption data is skewed, suggesting the presence of users with significantly higher consumption, which may represent industrial or commercial users.

3. **Optimal Number of Clusters**: Using the elbow method, we determined that the optimal number of clusters for our dataset is six. This provides a balance between model complexity and interpretability.

4. **Cluster Identification**: We applied KMeans clustering to identify distinct groups of users based on their consumption patterns. The clustering results, visualized through scatter plots, revealed clear distinctions between different user groups. These clusters can help the network operator understand the diversity in consumption patterns.

5. **User Characterization**: The identified clusters can be characterized by their consumption behaviors, which can aid in creating targeted demand management strategies. For instance, users in higher consumption clusters might benefit from energy-saving initiatives or differential pricing models.

6. **Demand Management Strategies**: By understanding the consumption patterns, the network operator can implement measures to reduce peak-hour congestion. For example, demand response programs can be tailored for users in specific clusters to shift their consumption to off-peak hours. The insights from the clustering analysis can also support the development of differential tariff schemes, ensuring they are aligned with the consumption behaviors of different user groups.

7. **Future Work**: Further analysis could include integrating additional data sources, such as weather or socio-economic data, to enhance the understanding of factors influencing consumption patterns. Advanced machine learning techniques can be explored to improve the accuracy and granularity of user segmentation.

These steps provide a solid foundation for understanding the consumption patterns of users and can help in formulating effective demand management strategies. The insights derived from this analysis can assist the network operator in optimizing their operations, reducing technical losses, and improving customer satisfaction through personalized services.
""")