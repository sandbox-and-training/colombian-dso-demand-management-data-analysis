# Colombian Distribution System Operator (DSO) Demand Management

## Overview

This repository showcases a basic data engineering and data science stages through a comprehensive project focused on demand management for a Network Operator (Utility) or also known as Distribution System Operator (DSO) in Colombia. The main objectives are to improve the use of distribution assets, reduce peak hour congestion, and explore potential differential tariff schemes.

## Repository Structure

- `data/`: Contains raw and processed datasets.
- `notebooks/`: Jupyter Notebooks for exploratory analysis and model development.
- `scripts/`: Python scripts for data processing and ETL tasks.
- `models/`: Saved models and related artifacts.
- `reports/`: Generated reports and visualizations.
- `README.md`: Project description, objectives, and instructions.

## Project Objectives

1. **Improve Asset Utilization**: Reduce technical losses in the distribution network and postpone capacity expansion projects.
2. **Characterize Users**: Understand user behaviors and characteristics to offer tailored products and services.
3. **Explore Tariff Schemes**: Evaluate potential differential tariff schemes for strategic regulatory discussions.

## Key Components

### Data Engineering

- **Data Ingestion**: Scripts to collect and ingest data from various sources.
- **Data Cleaning**: Preprocessing scripts to clean and transform the data.
- **ETL Pipeline**: Automated ETL pipeline setup using tools like Airflow or Prefect.

### Data Science

- **Exploratory Data Analysis (EDA)**: Notebooks detailing the initial data analysis and insights.
- **Clustering Analysis**: Application of K-means clustering to identify user consumption patterns.
- **Model Development**: Notebooks and scripts for model training, evaluation, and validation.
- **Visualization**: Creation of static and interactive visualizations to present findings.

## Data Sources

The data used in this project includes information collected from selected high-consumption users over a period of time. The datasets are stored in the `data/` directory.

## Notebooks

### [Demand_Management_Analysis.ipynb](notebooks/Demand_Management_Analysis.ipynb)

This notebook provides an in-depth analysis of the demand management problem, including data preprocessing, clustering analysis, and visualization of user consumption patterns.

## Scripts

### [data_ingestion.py](scripts/data_ingestion.py)

This script handles the data collection and ingestion process.

### [data_cleaning.py](scripts/data_cleaning.py)

This script is responsible for cleaning and preprocessing the data.

### [etl_pipeline.py](scripts/etl_pipeline.py)

This script sets up and automates the ETL pipeline.

## Models

The `models/` directory contains saved models and related artifacts used for clustering and analysis.

## Reports

The `reports/` directory contains generated reports and visualizations summarizing the findings and insights from the analysis.

## Getting Started

### Prerequisites

- Python 3.7 or higher
- Jupyter Notebook
- Required Python libraries (listed in `requirements.txt`)

### Installation

1. Clone the repository:
    ```bash
    git clone https://gitlab.com/sandbox-and-training/colombian-dso-demand-management-data-analysis.git
    ```
2. Navigate to the project directory:
    ```bash
    cd colombian-dso-demand-management-data-analysis
    ```
3. Install the required libraries:
    ```bash
    pip install -r requirements.txt
    ```

### Usage

1. Run the data ingestion script:
    ```bash
    python scripts/data_ingestion.py
    ```
2. Run the data cleaning script:
    ```bash
    python scripts/data_cleaning.py
    ```
3. Execute the ETL pipeline:
    ```bash
    python scripts/etl_pipeline.py
    ```
4. Open the Jupyter Notebook for analysis:
    ```bash
    jupyter notebook notebooks/Demand_Management_Analysis.ipynb
    ```

## Conclusion

This project demonstrates my expertise in both data engineering and data science through a practical demand management problem. By combining comprehensive data analysis, clustering techniques, and interactive visualizations, this repository showcases a robust approach to understanding and managing energy consumption patterns.